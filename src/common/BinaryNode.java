package common;

public class BinaryNode {
    public int data;
    public BinaryNode left, right;

    public BinaryNode(int data) {
        this.data = data;
        left = right = null;
    }

}
