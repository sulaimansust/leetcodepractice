package common;

public class BinaryTree {
    public BinaryNode root;

    public BinaryTree(BinaryNode root) {
        this.root = root;
    }

    public BinaryTree() {
        this.root = null;
    }

    public void addLeft(int data) {
        BinaryNode node = new BinaryNode(data);
        if (root != null) {
            root.left = node;
        } else {
            root = node;
        }
    }

    public void addRight(int data) {
        BinaryNode node = new BinaryNode(data);
        if (root != null) {
            root.right = node;
        } else {
            root = node;
        }
    }


}
