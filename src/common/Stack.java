package common;

/**
 * Created by sulaimankhan on 6/6/2017.
 */
public class Stack {
    int top;
    int[] numbers;
    public final int MAX_SIZE;

    public Stack(int max_size) {
        top = -1;
        MAX_SIZE = max_size;
        numbers = new int[MAX_SIZE];
    }

    public void push(int value) {
        if (top == MAX_SIZE - 1) {
            System.out.println("Stack is already fully loaded");
            return;
        }
        ++top;
        numbers[top] = value;
        return;
    }

    public int pop() {
        if (top>0)
            return numbers[top--];
        else {
            System.out.println("no element in the stack");
            return 0;
        }
    }

    public void print() {
        for (int i = 0; i <= top; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();
    }
}
