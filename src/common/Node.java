package common;

/**
 * Created by sulaimankhan on 4/9/2018.
 */
public class Node {
    public int data;
    public Node next=null;

    public Node(int data) {
        this.data = data;
    }

    public Node() {
    }
}
