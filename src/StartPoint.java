
import leetcode.easy.*;
import leetcode.linklist.AddTwoNumbers;
import leetcode.linklist.SinglyLinkList;
import leetcode.sorting.*;
import leetcode.string_related.LongestSubstringWuthoutRepeating;
import leetcode.string_related.ZigZagConversion;
import searching.BinarySearch;

import java.util.Random;

/**
 * Created by sulaimankhan on 01/06/2017.
 */
public class StartPoint {

    static void printArray(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }

    public static void main(String[] args) {
        SinglyLinkList singlyLinkList = new SinglyLinkList();
        SinglyLinkList singlyLinkList2 = new SinglyLinkList();
        /*for (int i = 1; i <= 3; i++) {
            singlyLinkList.add(i);
            singlyLinkList2.add(i + 1);

        }*/
        singlyLinkList.add(2);
        singlyLinkList.add(4);
        singlyLinkList.add(3);

        singlyLinkList2.add(5);
        singlyLinkList2.add(6);
        singlyLinkList2.add(4);

     /*   singlyLinkList.printAll();
        singlyLinkList2.printAll();*/
/*

        AddTwoNumbers addTwoNumbers = new AddTwoNumbers();
        addTwoNumbers.addTwoNumbers(singlyLinkList.getHead(),singlyLinkList2.getHead());
*/

        /*
        singlyLinkList.printAll();
        singlyLinkList2.printAll();
        singlyLinkList.reverseLinkList(singlyLinkList.getHead());
        singlyLinkList2.reverseLinkList(singlyLinkList2.getHead());
        singlyLinkList.printAll();
        singlyLinkList2.printAll();
        singlyLinkList.reverseBetween(singlyLinkList.getHead(),1,2);
        singlyLinkList.reverseLinkList(singlyLinkList.getHead());
        singlyLinkList.printAll();*/

        /*Stack stack = new Stack(100);

        for (int i=1;i<=100;i++){
            stack.push(i);
        }
        stack.print();

        Random random = new Random(100);

        for (int i=0;i<50;i++){
            if (random.nextBoolean()){
                stack.pop();
            }
        }
        stack.print();*/

/*
        int arr[] = {10, 7, 8, 9, 1, 5};
        int n = arr.length;

        QuickSort ob = new QuickSort();
        ob.sort(arr, 0, n - 1);

        System.out.println("sorted array");
        printArray(arr);*/
        int input2[] = {20, 18, 17, 16, 15, 14, 12, 13, 12};
        int input[] = {1, 2, 3, 4, 19, 20, 21, 5, 10, 17, 18, 22, 2331, 32, 33, 11, 12, 24, 25, 26, 27, 28, 29, 30, 34, 35, 13, 14, 6, 7, 8, 9, 15, 16,};
/*        BinarySearch binarySearch = new BinarySearch();
        int result = binarySearch.binarySearch(input, 0, input.length - 1, 5);

        if (result==1){
            System.out.println("Not found");
        } else{
            System.out.println("Value found in position: "+result);
        }*/
//
//        InsertionSort insertionSort = new InsertionSort();
//        insertionSort.insertionSort(input2);

/*        SelectionSort selectionSort = new SelectionSort();
        selectionSort.selectionSort(input2);*/

//        BubbleSort bubbleSort = new BubbleSort();
//        bubbleSort.bubbleSort(input2);


        /*QuickSort ob = new QuickSort();
        ob.sort(input2, 0, input2.length-1);

        System.out.println("sorted array");
        printArray(input2);*/

//        CountSort countSort = new CountSort();
//        countSort.countSort(input2,100);

  /*      NewQuickSort newQuickSort = new NewQuickSort();
        System.out.println(input2.length);
        newQuickSort.quickSort(input2,0,input2.length-1);
*/
      /*  MergeSort mergeSort = new MergeSort();
        mergeSort.mergeSort(input2,0,input2.length-1);
        for (int i:input2)
        {
            System.out.print(i+" ");
        }*/

      /*  LongestSubstringWuthoutRepeating longestSubstringWuthoutRepeating = new LongestSubstringWuthoutRepeating();
        longestSubstringWuthoutRepeating.lengthOfLongestSubstring("pwwkew");

        LongestSubstringWuthoutRepeating.longestUniqueSubsttr("pwwkew");*/

//        System.out.println(AddStrings.addStrings("999","999"));
//        System.out.println(ArrangingCoins.arrangeCoins(54));


//        int array[] = {1,2,2,1,2,3,4,2,5,2,1,3,2,5};
//        System.out.println("count ===== "+ RemoveElementFromArray.removeElement(array,2));

        ZigZagConversion.convert("PAYPALISHIRING", 3);

    }
}
