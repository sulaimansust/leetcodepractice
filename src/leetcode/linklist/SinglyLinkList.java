package leetcode.linklist;

import org.w3c.dom.NodeList;

import javax.sound.midi.Soundbank;

/**
 * Created by sulaimankhan on 02/06/2017.
 */
public class SinglyLinkList {

    private ListNode head;

    public SinglyLinkList() {
        this.head = null;
    }

    public ListNode getHead() {
        return head;
    }

    public void add(int val) {
        ListNode listNode = new ListNode(val);
        listNode.next = null;
        if (head == null) {
            head = listNode;
            return;
        }
        ListNode temp = head;
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = listNode;
    }

    public void printAll() {
        ListNode temp = head;
        while (temp!=null) {
            System.out.print(temp.val + " ");
            temp = temp.next;
        }
        System.out.println();


    }

    public void setHead(ListNode head) {
        this.head = head;
    }

    public ListNode reverseBetween(ListNode head, int m, int n) {

        int tempM=m;

        if (head==null || head.next==null){
            return head;
        }


        ListNode start,end, reverseStart, reverseEnd;

        ListNode tempHead=head;

        for (int i=2;i<m;i++){
            tempHead=tempHead.next;
        }
        start = tempHead;
        System.out.println("start: "+start.val);

        ListNode prev=tempHead.next;
        m++;

        reverseStart=prev;
        System.out.println("reverse start: "+reverseStart.val);
        ListNode next = prev.next;
        m++;
        while(m<=n){
            ListNode temp = next.next;
            next.next = prev;
            prev = next;
            next = temp;
            m++;
        }
        end = next.next;
        System.out.println(" end: "+end.val);
        next.next=prev;
        reverseEnd=next;

        System.out.println("reverse end: "+reverseEnd.val);
        reverseStart.next = end;
        if (tempM!=1) {
            start.next = reverseEnd;
        } else{
            head= reverseEnd;
        }

        return head;
    }

    public ListNode reverseLinkList(ListNode head) {
        if (head == null || head.next==null) {
            return head;
        }
        ListNode prev = head;
        ListNode next = head.next;
        prev.next = null;

        while (next.next != null) {
            ListNode temp = next.next;
            next.next = prev;
            prev = next;
            next = temp;
        }
        next.next = prev;
        this.head=next;
        return next;
    }

    public ListNode addTwoLinkList(ListNode l1, ListNode l2){

        if (l1==null){
            return l2;
        } else if (l2==null){
            return l1;
        }


        ListNode head;
        int valueInHand = 0;
        if (l1.val+l2.val>=10){
            head = new ListNode((l1.val+l2.val)%10);
            valueInHand=1;
            System.out.println("new node value: "+head.val+" valueInHand: "+valueInHand);
        } else {
            head = new ListNode((l1.val+l2.val));
        }
        l1=l1.next;
        l2=l2.next;
        ListNode endPosition = head;

        while (l1!=null && l2!=null){
            int val1=l1.val;
            int val2 = l2.val;
            ListNode temp;
            if (val1+val2+valueInHand>10){
                temp = new ListNode((val1+val2+valueInHand)%10);
                valueInHand=1;
            } else{
                temp = new ListNode((val1+val2+valueInHand));
                valueInHand=0;
            }
            endPosition.next=temp;
            endPosition = temp;
            l1=l1.next;
            l2=l2.next;
        }
        if (l1!=null){
            while (l1!=null){
                endPosition.next=l1;
                endPosition=l1;
                l1=l1.next;
            }
        } else if (l2!=null){
            while (l2!=null){
                endPosition.next=l2;
                endPosition=l2;
                l2=l2.next;
            }
        }

        return head;
    }


}
