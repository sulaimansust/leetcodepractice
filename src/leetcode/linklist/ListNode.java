package leetcode.linklist;

/**
 * Created by sulaimankhan on 02/06/2017.
 */


public class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
}
