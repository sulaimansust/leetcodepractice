package leetcode.linklist;

/**
 * Created by sulaiman on 10/9/2017.
 */
public class AddTwoNumbers {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int count=0;
        ListNode list1=l1, list2=l2;
        int sumOfTwoNode, valueOnHand=0;
        ListNode head=null,currentHead=null, temp;

        while(list1!=null || list2!=null){
            sumOfTwoNode = valueOnHand;
            if (list1!=null){
                sumOfTwoNode+=list1.val;
                list1=list1.next;
            }
            if (list2!=null){
                sumOfTwoNode+=list2.val;
                list2=list2.next;
            }
            if (sumOfTwoNode>=10){
                sumOfTwoNode=sumOfTwoNode%10;
                valueOnHand=1;
            } else {
                valueOnHand=0;
            }
            temp = new ListNode(sumOfTwoNode);
            if (head==null){
                head = temp;
                currentHead=temp;
            } else{
                currentHead.next = temp;
                currentHead=temp;
            }
        }
        if (valueOnHand!=0){
            currentHead.next = new ListNode(valueOnHand);
        }
        while(head!=null){
            System.out.print(head.val+" ");
            head=head.next;
        }


        return currentHead;
    }

}
