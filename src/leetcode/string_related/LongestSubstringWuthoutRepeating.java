package leetcode.string_related;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by sulaiman on 10/9/2017.
 */
public class LongestSubstringWuthoutRepeating {

    public int lengthOfLongestSubstring(String s, int a) {
        char input[] = s.toCharArray();
        int max=0;
        List<Character> wordList = new LinkedList<>();
        for (int i=0;i<input.length;i++){
            int stringCount=0;
            wordList.clear();
            for (int j=i;j<input.length;j++){
                if (wordList.contains(input[j])){
                    break;
                }
                wordList.add(input[j]);
                stringCount++;
            }
            if (stringCount>max){
                max=stringCount;
            }
        }
        System.out.println("=============== "+max);
        return max;
    }

    public int lengthOfLongestSubstring(String s) {
        System.out.println(s.charAt(0));
        char input[] = s.toCharArray();
        int max=0,currentMax=0,prev_index;

        int visited[] = new int[256];
        for (int i=0;i<256;i++){
            visited[i]=-1;
        }

        visited[input[0]]=0;

        return max;
    }
    public static int longestUniqueSubsttr(String str)
    {
        int n = str.length();
        int cur_len = 1;    // length of current substring
        int max_len = 1;    // result
        int prev_index;        //  previous index
        int i;
        int visited[] = new int[256];

        /* Initialize the visited array as -1, -1 is
         used to indicate that character has not been
         visited yet. */
        for (i = 0; i < 256; i++) {
            visited[i] = -1;
        }

        /* Mark first character as visited by storing the
             index of first   character in visited array. */
        visited[str.charAt(0)] = 0;

        /* Start from the second character. First character is
           already processed (cur_len and max_len are initialized
         as 1, and visited[str[0]] is set */
        for(i = 1; i < n; i++)
        {
            prev_index = visited[str.charAt(i)];

            /* If the current character is not present in
           the already processed substring or it is not
              part of the current NRCS, then do cur_len++ */
            System.out.println(str.charAt(i)+" \ni: "+i+" max_len: "+max_len+" cur_len: "+cur_len+" i-cur_len: "+(i-cur_len)+" prev_index: "+prev_index);
            if(prev_index == -1 || i - cur_len > prev_index) {
                cur_len++;
                System.out.println("if");
            }
            /* If the current character is present in currently
               considered NRCS, then update NRCS to start from
               the next character of previous instance. */
            else
            {
                System.out.println("else");
                /* Also, when we are changing the NRCS, we
                   should also check whether length of the
                   previous NRCS was greater than max_len or
                   not.*/
                if(cur_len > max_len)
                    max_len = cur_len;

                cur_len = i - prev_index;
            }

            // update the index of current character
            visited[str.charAt(i)] = i;
            System.out.println(str.charAt(i)+" \ni: "+i+" max_len: "+max_len+" cur_len: "+cur_len+" i-cur_len: "+(i-cur_len)+" prev_index: "+prev_index+"\n\n\n");
        }

        // Compare the length of last NRCS with max_len and
        // update max_len if needed
        if(cur_len > max_len)
            max_len = cur_len;

        return max_len;
    }

}
