package leetcode.string_related;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ZigZagConversion {

    public static String convert(String inputString, int lengthOfZigZag) {

        char input[] = inputString.toCharArray();
        ArrayList tempList[] = new ArrayList[lengthOfZigZag];


         for (int i=0;i<lengthOfZigZag;i++) {
             tempList[i] = new ArrayList<String>();
         }

         for(int i=0;i<input.length;i++) {
             int position = i%lengthOfZigZag;

             tempList[position].add(input[i]);
         }

        for (int i = 0; i < tempList.length; i++) {

            for (Object s: tempList[i]){
                System.out.println(s);
            }
            System.out.println("\n\n\n");

        }



        return "";
    }

}
