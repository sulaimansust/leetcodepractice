package leetcode;

import java.util.ArrayList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * Created by sulaimankhan on 02/06/2017.
 */
public class StringReverse {
    public String reverseVowel(String s) {
        char[] strings = s.toCharArray();
        int[] position = new int[s.length()];
        int count = 0;
        char[] vowels = new char[s.length()];
        for (int i = 0; i < strings.length; i++) {
            switch (strings[i]) {
                case 'a':
                    vowels[count] = strings[i];
                    position[count++] = i;
                    break;
                case 'A':
                    vowels[count] = strings[i];
                    position[count++] = i;
                    break;
                case 'e':
                    vowels[count] = strings[i];
                    position[count++] = i;
                    break;
                case 'E':
                    vowels[count] = strings[i];
                    position[count++] = i;
                    break;
                case 'i':
                    vowels[count] = strings[i];
                    position[count++] = i;
                    break;
                case 'I':
                    vowels[count] = strings[i];
                    position[count++] = i;
                    break;
                case 'o':
                    vowels[count] = strings[i];
                    position[count++] = i;
                    break;
                case 'O':
                    vowels[count] = strings[i];
                    position[count++] = i;
                    break;
                case 'u':
                    vowels[count] = strings[i];
                    position[count++] = i;
                    break;
                case 'U':
                    vowels[count] = strings[i];
                    position[count++] = i;
                    break;
            }
        }

        for (int i = 0; i < count; i++) {
            strings[position[i]] = vowels[count - i - 1];
        }

        return new String(strings);
    }

    public String reverseString(String s) {
        StringBuilder stringBuilder = new StringBuilder(s);
        return stringBuilder.reverse().toString();
    }

    //557. Reverse Words in a String III
    public void reverseArrayFromPosition(char[] text, int start, int end) {
        System.out.println("start: "+start+" end: "+end);
        if (start==end || end<0){
           return;
        }
        for (int i = start; i <= ((start + end) / 2); i++) {
            char temp = text[i];
            text[i] = text[end - i+start];
            text[end - i+start] = temp;
        }

    }
    //557. Reverse Words in a String III
   /* public String reverseWords(String s) {

        int length = s.length();
        char[] text = s.toCharArray();
        if (length==0){
            return s;
        }
        int[] position = new int[length];
        int count = 0;
        for (int i = 0; i < length; i++) {
            if (text[i] == ' ') {
                position[count] = i;
                count++;
            }
        }
        for (int i = 0; i < count; i++) {
            if (i == 0) {
                reverseArrayFromPosition(text, 0, position[i]-1);
            } else {
                reverseArrayFromPosition(text, position[i - 1] + 1, position[i]-1);
            }
        }
        if (count>=1)
            reverseArrayFromPosition(text,position[count-1]+1,s.length()-1);

        if (count==0){
            reverseArrayFromPosition(text,0,text.length-1);
        }


        return new String(text);
    }*/
    //Leetcode 541. Reverse String II
    /*Given a string and an integer k, you need to reverse the first k characters for every 2k characters counting from the start of the string.
    If there are less than k characters left, reverse all of them. If there are less than 2k but greater than or equal to k characters, then
    reverse the first k characters and left the other as original.*/

    public String reverseStr(String s, int k) {

        char[] texts = s.toCharArray();

        int length = s.length();
        int currentPosition=0;

        for(int i=0;i<=length/(2*k);i++){
            if (length>0){
                reverseArrayFromPosition(texts,currentPosition,currentPosition+k>=length?length-1:currentPosition+k-1);
                currentPosition +=2*k;
            }
        }
        return new String(texts);
    }


    /*public void reverseArrayFromPosition(char[] text, int start, int end) {
        for (int i = start; i <= ((start + end) / 2); i++) {
            char temp = text[i];
            text[i] = text[end - i+start];
            text[end - i+start] = temp;
        }

    }*/

    //151. Reverse Words in a String ***************** I need to solve it later

    public String copyWordFromPosition(String s){
        int length = s.length();
        char[] text = s.toCharArray();
        if (length==0){
            return s;
        }
        reverseArrayFromPosition(text,0,s.length()-1);
        int[] position = new int[length];
        int count = 0;
        for (int i = 0; i < length; i++) {
            if (text[i] == ' ') {
                position[count] = i;
                count++;
            }
        }
        for (int i = 0; i < count; i++) {
            if (i == 0) {
                reverseArrayFromPosition(text, 0, position[i]-1);
            } else {
                reverseArrayFromPosition(text, position[i - 1] + 1, position[i]-1);
            }
        }
        if (count>=1)
            reverseArrayFromPosition(text,position[count-1]+1,s.length()-1);

        if (count==0){
            reverseArrayFromPosition(text,0,text.length-1);
        }


        return new String(text);
    }



}
