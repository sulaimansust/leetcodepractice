package leetcode;

/**
 * Created by sulaimankhan on 01/06/2017.
 */
public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        int[] returnValue = new int[2];
        int sum,i=0,j=nums.length-1,value1=0,value2=0;
        int[] tempArray = new int[nums.length];
        for(int m=0;m<nums.length;m++){
            tempArray[m]=nums[m];
        }
        for (int m=0;m<nums.length;m++){
            for (int n=m+1;n<nums.length;n++){
                if (nums[m]>nums[n]){
                    int temp = nums[m];
                    nums[m]=nums[n];
                    nums[n]=temp;
                }
            }
        }

        for (int z: nums) {
            System.out.print(z+" ");
        }
        System.out.println();

        for(int k=0;k<nums.length;k++){
            sum = nums[i]+nums[j];
            if (sum == target){
                value1=nums[i];
                value2=nums[j];
                break;
            } else if (sum>target){
                j--;
            } else{
                i++;
            }
        }
        int count=0;
        for (int q=0;q<nums.length;q++){
            if (tempArray[q]==value1 || tempArray[q]==value2){
                returnValue[count++]=q;
            }
        }


        return returnValue;
    }
}
