package leetcode.sorting;

/**
 * Created by sulaimansust on 6/16/17.
 */
public class SelectionSort {

    public void selectionSort(int[] input) {
        for (int i = 0; i < input.length; i++) {
            for (int j = i + 1; j < input.length; j++) {
                if (input[i] > input[j]) {
                    int temp = input[i];
                    input[i] = input[j];
                    input[j] = temp;
                }
                for (int m:input){
                    System.out.print(m+" ");
                }
                System.out.println();
            }
            System.out.println("========================================================");
        }

        for (int i:input){
            System.out.print(i+" ");
        }
    }
}
