package leetcode.sorting;

/**
 * Created by sulaimansust on 6/16/17.
 */
public class MergeSort {
    public void mergeSort(int[] input, int start, int end) {
        if (start < end) {
            int mid = (start + end) / 2;

            mergeSort(input, start, mid);
            mergeSort(input, mid + 1, end);
            merge(input, start, mid, end);
        }
    }

    private void merge(int[] input, int start, int mid, int end) {
        int num1 = mid-start+1;
        int num2 = end-mid;
        int[] leftArray = new int[num1];
        int[] rightArray = new int[num2];
        
        for(int i=0;i<num1;i++){
            leftArray[i]=input[start+i];
        }


        for(int i=0;i<num2;i++){
            rightArray[i]=input[mid+i+1];
        }
        
        int left=0,right=0;
        int k = start;
        
        while (left<num1 && right<num2){
            if (leftArray[left]<rightArray[right]){
                input[k++] = leftArray[left++];
            } else {
                input[k++]=rightArray[right++];
            }
        }
        
        while (left<num1){
            input[k++]=leftArray[left++];
        }
        while (right<num2){
            input[k++]=rightArray[right++];
        }
        
    }
}
