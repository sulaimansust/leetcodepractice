package leetcode.sorting;

/**
 * Created by sulaimansust on 6/16/17.
 */
public class CountSort {
    public void countSort(int[] array,int maxValue){
        int[] storage = new int[maxValue+1];

        for (int i: array){
            storage[i]+=1;
        }
        int count=0;
        for (int i=0;i<storage.length;i++){
            for (int j=0;j<storage[i];j++){
                array[count++]=i;
            }
        }

        for (int i:array){
            System.out.print(i+" ");
        }
    }
}
