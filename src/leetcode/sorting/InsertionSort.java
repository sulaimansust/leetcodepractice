package leetcode.sorting;

/**
 * Created by sulaimansust on 6/16/17.
 */
public class InsertionSort {
    public void insertionSort(int[] input){
        int key,j;

        for (int i=0;i<input.length;i++){
            key = input[i];
            j=i-1;
            System.out.println("i: "+i+" data: "+key+" j: "+j);
            while (j>=0 && input[j]>key){
                input[j+1]=input[j];
                j--;
                System.out.println("Shifting");
            }
            input[j+1]=key;
            System.out.println("data "+key+" inserted in position: "+(j+1));
        }

        for (int i:input){
            System.out.print(i+" ");
        }
    }
}
