package leetcode.sorting;

/**
 * Created by sulaimansust on 6/16/17.
 */
public class NewQuickSort {

    public void quickSort(int[] input,int low, int high){
        System.out.println("quickSort high: "+high+" low: "+low);
        if (low<high) {
            int partitionIndex = partition(input, low, high);
            quickSort(input, low, partitionIndex - 1);
            quickSort(input, partitionIndex + 1, high);
        }
    }

    public int partition(int[] input,int low, int high){
        System.out.println("partition high: "+high+" low: "+low);
        int pivot = input[high];
        int j=low-1;

        for (int i=low;i<high;i++){
            if (input[i]<pivot){
                j++;
                int temp = input[i];
                input[i]=input[j];
                input[j]=temp;
            }
        }
        int temp = input[high];
        input[high]=input[j+1];
        input[j+1]=temp;

        return j+1;
    }
}
