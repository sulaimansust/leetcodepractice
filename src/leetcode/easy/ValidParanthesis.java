package leetcode.easy;

import java.util.Stack;

/**
 * Created by sulaimankhan on 12/1/2017.
 */
public class ValidParanthesis {
    public static boolean isValid(String s) {

        if (s.length() % 2 == 1)
            return false;

        Stack stack = new Stack();

        char[] data = s.toCharArray();
        boolean isValid = true;
        for (int i = 0; i < data.length; i++) {
            if (data[i] == '(' || data[i] == '{' || data[i] == '[') {
                stack.push(data[i]);
            } else {
                if (stack.isEmpty()) return false;
                char currentTop = (char) stack.pop();
                switch (currentTop) {
                    case '(':
                        if (data[i] == ')')
                            continue;
                        else return false;
                    case '{':
                        if (data[i] == '}')
                            continue;
                        else return false;

                    case '[':
                        if (data[i] == ']')
                            continue;
                        else return false;
                }
            }
        }

        if (stack.isEmpty())
            return isValid;
        else return false;
    }
}
