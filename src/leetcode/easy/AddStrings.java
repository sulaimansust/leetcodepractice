package leetcode.easy;

public class AddStrings {
    public static  String addStrings(String a, String b) {
        if (a == null || a.equals(""))
            return b;
        if (b == null || b.equals("")) {
            return a;
        }

        StringBuilder result = new StringBuilder();

        char[] aa = a.toCharArray();
        char[] bb = b.toCharArray();

        int lengthA = aa.length-1;
        int lengthB = bb.length-1;
        int  valueOnHand = 0;
        while (lengthA >= 0 || lengthB >= 0) {
            int tempSum = 0;
            tempSum += valueOnHand;
            valueOnHand = 0;

            if (lengthA >= 0) {
                tempSum += aa[lengthA]-48;
                lengthA--;
            }

            if (lengthB >= 0) {
                tempSum += bb[lengthB]-48;
                lengthB--;
            }
            if (tempSum >= 10) {
                result.append(String.valueOf(tempSum-10));
                valueOnHand = tempSum/10;
            } else {
                result.append(String.valueOf(tempSum));
                valueOnHand=0;
            }
        }
        if (valueOnHand>0){
            result.append(String.valueOf(valueOnHand));
        }

        return result.reverse().toString();
    }
}
