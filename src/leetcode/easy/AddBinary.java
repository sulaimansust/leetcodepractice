package leetcode.easy;

//https://leetcode.com/problems/add-binary/description/
public class AddBinary {


    public static String addBinary(String a, String b) {
        System.out.println(a+" "+b);
        if (a == null || a.equals(""))
            return b;
        if (b == null || b.equals("")) {
            return a;
        }

        StringBuilder result = new StringBuilder();

        char[] aa = a.toCharArray();
        char[] bb = b.toCharArray();

        int lengthA = aa.length-1;
        int lengthB = bb.length-1;
        int  valueOnHand = 0;
        while (lengthA >= 0 || lengthB >= 0) {
            int tempSum = 0;
            tempSum += valueOnHand;
            valueOnHand = 0;

            if (lengthA >= 0) {
                tempSum += aa[lengthA] == '1' ? 1 : 0;
                lengthA--;
            }

            if (lengthB >= 0) {
                tempSum += bb[lengthB] == '1' ? 1 : 0;
                lengthB--;
            }
            if (tempSum >= 2) {
                result.append(String.valueOf(tempSum-2));
                valueOnHand = 1;
            } else {
                result.append(String.valueOf(tempSum));
                valueOnHand=0;
            }
        }
        if (valueOnHand==1){
            result.append(String.valueOf(valueOnHand));
        }

        return result.reverse().toString();
    }
}
