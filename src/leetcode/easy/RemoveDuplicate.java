package leetcode.easy;

public class RemoveDuplicate {

    public static int removeDuplicates(int[] data){

        if (data.length<=1)
            return data.length;

        int count=1;
        int position=1;

        Integer lastValue=null;


        for (int i=1;i<data.length;i++){
            if (data[i]==data[i-1]) {
                if (lastValue==null){
                    lastValue=data[i];
                    position=i;
                } else if(lastValue!=data[i]){
                    lastValue=data[i];
                }
                continue;
            }
            data[position++]=data[i];
            count++;
        }
        return count;

    }
}
