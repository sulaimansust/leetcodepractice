package leetcode.easy;

public class AddDigits {
    public static int addDigits(int num) {

        int temp = 0;

        while (num >= 10) {
            temp+=num%10;
            num /= 10;
            if (num<10){
                num = num+temp;
                temp=0;
            }
        }

        return num;
    }
}
