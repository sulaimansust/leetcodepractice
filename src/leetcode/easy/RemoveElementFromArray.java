package leetcode.easy;

/**
 * Created by sulaimankhan on 12/1/2017.
 */
public class RemoveElementFromArray {
    public static int removeElement(int[] nums, int val) {

        int count=0;
        Integer position=null;
        for (int i=0;i<nums.length;i++){
            if (nums[i]==val){

                if (position==null){
                    position=i;
                }
                continue;
            }
            if (position!=null){
                nums[position++]=nums[i];
            }
            ++count;

        }
        return count;
    }
}
