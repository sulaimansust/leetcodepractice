package hackerrank;

import common.BinaryNode;
import common.BinaryTree;
import common.Node;

import hackerrank.cracking_coding_interview.ArrayRotation;
import hackerrank.cracking_coding_interview.BinarySearchTree;
import hackerrank.datastructure.linkedlist.*;
import hackerrank.datastructure.tree.TreeOperation;

public class HackerMain {
    public static void main(String[] args) {

        //For hour glass

//        int input[][] = {
//                {1,1,1,0,0,0},
//                {0,1,0,0,0,0},
//                {1,1,1,0,0,0},
//                {0,0,2,4,4,0},
//                {0,0,0,2,0,0},
//                {0,0,1,2,4,0}
//        };
//
//        System.out.println(HourGlass.getMaxHourGlass(input));

        //Left Rotation
//        int input[] = {1,2,3,4,5};
//
//        LeftRotation.rotateLeft(input,3);
//
//        printArray(input);


        //DynamicArray

//        String[] strings = {"aba","baba","aba","xzxb"};
//
//        System.out.println(DynamicArray.findSuffix(strings,"aba"));


            //Array Manipulation

//        ArrayManipulation.arrayManipulation(5,new int[1][1]);

//        int [][] queries = {
//                {1,2,100},
//                {2,5,100},
//                {3,4,100}
//        }       ;
//
//        ArrayManipulation.arrayManipulation(5,queries);

        //

        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);
        head.next.next.next.next = new Node(5);
        head.next.next.next.next.next = new Node(6);

        Node head2 = new Node(1);
        head2.next = new Node(2);
        head2.next.next = new Node(3);
        head2.next.next.next = new Node(4);
        head2.next.next.next.next = new Node(7);


//        System.out.println(PrintLinkedList.compareTwoLinkList(head,head2));
//
//        System.out.println("--------");
//        PrintLinkedList.print(head);
//        System.out.println("--------");
//        BinaryNode node = PrintLinkedList.insertNth(head,0,2);

//        BinaryNode node2 = PrintLinkedList.deleteNodeInPosition(head,2);
//
//        BinaryNode node = PrintLinkedList.reverseLinkedList(head);
//
//        PrintLinkedList.print(node);
//        System.out.println("--------");
//        PrintLinkedList.printReverse(head);

//        Node newHead = PrintLinkedList.mergeTwoSortedLinkedList(head,head2);
//
//        PrintLinkedList.print(newHead);


        int data[] = {1,2,3,4,5,6};

//       printArray( ArrayRotation.rotateLeft(data,2) );

        BinaryTree tree = new BinaryTree(new BinaryNode(4));
        tree.root.left  = new BinaryNode(2);
        tree.root.left.left = new BinaryNode(1);
        tree.root.left.right = new BinaryNode(3);

        tree.root.right = new BinaryNode(6);
        tree.root.right.left = new BinaryNode(5);
        tree.root.right.right = new BinaryNode(7);


        System.out.println(    BinarySearchTree.checkBST(tree.root) );
//        int data[] = {1,2,3,4,5,6};
//
//       printArray( ArrayRotation.rotateLeft(data,2) );


    }

    static void printArray(int input[]) {
        for (int i: input) {
            System.out.print(" "+i);
        }
        System.out.println();
    }
}
