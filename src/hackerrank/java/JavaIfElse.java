package hackerrank.java;

import java.util.Scanner;

/**
 * Created by sulaimankhan on 4/15/2018.
 */
public class JavaIfElse {

    static {
        Scanner scanner = new Scanner(System.in);

        try {
            int breadth = scanner.nextInt();
            int height = scanner.nextInt();

            if (breadth<0 || height<0){
                System.out.println("java.lang.Exception: Breadth and height must be positive");
            } else {
                System.out.println(breadth*height);
            }


        } catch (Exception e) {
            System.out.println("java.lang.Exception: Breadth and height must be positive");
        }


    }

    public static void main(String[] args) {

    }

    static int diagonalDifference(int[][] a) {

        int difference = 0, length = a.length;

        int sum1=0, sum2=0;
        int start1=0;
        int start2 = length-1;

        for (int i=0;i<length;i++) {
            sum1 += a[start1][i];
            sum2 += a[start2][i];
            start1++;
            start2--;
        }

        difference = -sum1 + sum2;


        return difference;

    }
}
