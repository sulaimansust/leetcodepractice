package hackerrank.datastructure.stacks;


import java.util.Stack;

/*
* Valid Parentheses in leetcode
*
*
* */
public class BalancedBracket {

    static String isBalanced(String s) {
        if (s.length() % 2 == 1)
            return "NO";

        Stack stack = new Stack();

        char[] data = s.toCharArray();
        boolean isValid = true;
        for (int i = 0; i < data.length; i++) {
            if (data[i] == '(' || data[i] == '{' || data[i] == '[') {
                stack.push(data[i]);
            } else {
                if (stack.isEmpty()) return "NO";
                char currentTop = (char) stack.pop();
                switch (currentTop) {
                    case '(':
                        if (data[i] == ')')
                            continue;
                        else return "NO";
                    case '{':
                        if (data[i] == '}')
                            continue;
                        else return "NO";

                    case '[':
                        if (data[i] == ']')
                            continue;
                        else return "NO";
                }
            }
        }

        if (stack.isEmpty())
            return isValid?"YES":"NO";
        else return "NO";
    }

}
