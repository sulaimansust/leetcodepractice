package hackerrank.datastructure.array;

public class DynamicArray {

    public static int findSuffix(String[] collections, String queryString) {

        int count = 0;
        for (String value : collections) {
            if(value.equals(queryString)){
                count++;
            }

        }

        return count;

    }

}
