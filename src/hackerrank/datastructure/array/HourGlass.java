package hackerrank.datastructure.array;

/*
*
* https://www.hackerrank.com/challenges/2d-array/problem
* */

public class HourGlass {

    public static int getMaxHourGlass(int input[][]) {

        int rowLength = input.length;
        int columnLength = input[0].length;

        int result=0;
        int sum=Integer.MIN_VALUE;
        for (int row=1;row<rowLength-1;row++) {
            System.out.println("=============");
            for (int column=1;column<columnLength-1;column++) {

                sum+=input[row][column];

                sum += input[row-1][column-1];
                sum += input[row-1][column];
                sum += input[row-1][column+1];

                sum += input[row+1][column-1];
                sum += input[row+1][column];
                sum += input[row+1][column+1];

                System.out.println("result: "+result+" sum: "+sum);

                if (sum > result) {
                    result = sum;
                }
                sum = 0;

            }
        }

        return result;
    }

}
