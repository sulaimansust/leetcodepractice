package hackerrank.datastructure.array;

public class ArrayManipulation {


    public static int arrayManipulation(int n,int[][]queries) {

        int result[] = new int[n+1];



        for (int[] query: queries){

            for (int i=query[0];i<=query[1];i++) {
                result[i]+=query[2];
            }

        }

        for (int val:result){
            System.out.print(val+" ");
        }

        System.out.println();

        int max = Integer.MIN_VALUE;

        for (int value = 1; value<result.length;value++) {
            if (result[value]>max) {
                max = result[value];
            }
        }

        System.out.println(max);

        return max;
    }

}
