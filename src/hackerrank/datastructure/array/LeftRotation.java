package hackerrank.datastructure.array;

public class LeftRotation {
    public static void rotateLeft(int input[], int noOfRotation) {

        int length = input.length;
        noOfRotation = noOfRotation%length;

        int temp[] = new int[noOfRotation];

        for (int i=0;i<noOfRotation;i++) {
            temp[i] = input[i];
        }
        int start = 0;
        for (int i=noOfRotation;i<length;i++) {
            input[start++] = input[i];
        }
        start=0;
        for (int i=length-noOfRotation;i<length;i++) {
            input[i] = temp[start++];
        }

        for (int i: input) {
            System.out.print(i+" ");
        }

    }
}
