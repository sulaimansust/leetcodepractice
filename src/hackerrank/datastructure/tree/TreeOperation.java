package hackerrank.datastructure.tree;

import java.util.*;

public class TreeOperation {

    static class Node {
        int data;
        Node left;
        Node right;

        Node() {

        }

        Node(int data) {
            this.data = data;
            left = right = null;
        }
    }

    static void preorderTraversal(Node root) {
        if (root == null) return;
        System.out.print(root.data + " ");
        preorderTraversal(root.left);
        preorderTraversal(root.right);
    }

    void postOrder(Node root) {
        if (root == null) return;
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.data + " ");

    }

    void inOrder(Node root) {
        if (root == null) return;
        inOrder(root.left);
        System.out.print(root.data + " ");
        inOrder(root.right);
    }

    /*
     * Problem : Tree: Height of a Binary Tree
     * Link: https://www.hackerrank.com/challenges/tree-height-of-a-binary-tree/problem
     * */

    static int traverse(Node root, int height) {
        if (root == null) return height;
        height++;
        int leftHeight = traverse(root.left, height);
        int rightHeight = traverse(root.right, height);

        return leftHeight > rightHeight ? leftHeight : rightHeight;
    }

    public static int height(Node root) {
        int height = 0;

        height = traverse(root, height);

        System.out.println(height);
        return height;
    }
    /*
     *
     * Problem : Tree : Top View
     *
     * */

    void topView(Node root) {
        if (root == null) return;
        System.out.print(root.data + " ");
        topView(root.right);

    }

    static void levelOrder(Node root) {


        List<Node> queue = new ArrayList<>();
        queue.add(root);


        while (queue != null && !queue.isEmpty()) {
            Node temp = queue.remove(0);
            if (temp != null) {
                System.out.print(temp.data + " ");
                queue.add(temp.left);
                queue.add(temp.right);
            }

        }


    }

    /*
     * This one is a BST : Need to insert
     *
     *
     * */

    static Node Insert(Node root, int value) {

        Node newNode = new Node();
        newNode.data = value;

        if (root == null) {
            root = newNode;
        }
        Node temp = root;

        while (temp != null) {
            if (value > temp.data) {

                if (temp.right != null)
                    temp = temp.right;
                else
                    temp.right = newNode;
            } else {
                if (temp.left != null)
                    temp = temp.left;
                else
                    temp = newNode;
            }

        }


        return root;

    }

    public static void main(String[] args) {

        Node root = new Node(3);
        root.left = new Node(2);
        root.left.left = new Node(1);


        root.right = new Node(5);
        root.right.left = new Node(4);
        root.right.right = new Node(6);
        root.right.right.right = new Node(7);
//        height(root);

//        preorderTraversal(root);
        levelOrder(root);


    }


}
