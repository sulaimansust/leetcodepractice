package hackerrank.datastructure.tree;

public class BinarySearchTree {

    public class Node {
        int data;
        Node left, right;

        public Node(int data) {
            this.data = data;
            this.left = null;
            this.right = null;
        }

    }

    private Node root;


    public BinarySearchTree() {
        this.root = null;
    }

    public BinarySearchTree(Node root) {
        this.root = root;
    }


    public Node insertNode(int data) {
        if (root == null) {
            root = new Node(data);
        } else {
            Node current = root;
            Node parent = current;
            while (current != null) {
                parent = current;
                if (data > current.data) {
                    current = current.right;
                } else {
                    current = current.left;
                }
            }

            if (parent.data < data) {
                parent.right = new Node(data);
            } else {
                parent.left = new Node(data);
            }


        }
        return root;
    }

    public boolean findNode(int value) {

        Node current = root;

        while (current != null) {
            if (current.data == value) return true;

            if (value > current.data) {
                current = current.right;
            } else {
                current = current.left;
            }
        }


        return false;
    }

    public Node findAndDeleteMinimumNodeFromANode(Node root){


        Node parent = null;
        Node current = root;


        while (current.left!=null){
            parent = current;
            current  = current.left;
        }



        return null;
    }


    public Node delete(int value) {


        Node current = root;
        Node parent = null;

        while (current != null) {
            if (current.data == value) {
                if (current.left == null && current.right == null) {
                    if (parent != null) {
                        if (value > parent.data) {
                            parent.right = null;
                        } else {
                            parent.left = null;
                        }
                        current = null;
                    } else {
                        return null;
                    }
                } else if ((current.left == null & current.right != null) || (current.left != null && current.right == null)) {

                    if (parent != null) {
                        if (value > parent.data) {
                            parent.right = current.left == null ? current.right : current.left;
                        } else {
                            parent.left = current.left == null ? current.right : current.left;
                        }
                        current = null;
                    } else {
                        parent = current;
                        current =  current.left==null?current.right:current.left;
                        parent = null;

                        root = current;
                    }


                } else {

                }
            }
        }


        return root;
    }


}
