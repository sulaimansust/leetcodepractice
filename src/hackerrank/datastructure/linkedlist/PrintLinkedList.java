package hackerrank.datastructure.linkedlist;

import common.Node;

/**
 * Created by sulaimankhan on 4/9/2018.
 */
public class PrintLinkedList {

    //problem 1 : https://www.hackerrank.com/challenges/print-the-elements-of-a-linked-list/problem
    public static void print(Node head) {
        while (head != null) {
            System.out.println(head.data);
            head = head.next;
        }
    }

    //Problem 2: https://www.hackerrank.com/challenges/insert-a-node-at-the-tail-of-a-linked-list/problem
    public static Node insertAtTail(Node head, int data) {

        Node node = new Node();
        node.data = data;

        if (head == null) {
            head = node;
            return head;
        }

        Node temp = head;

        while (temp.next != null) {
            temp = temp.next;
        }

        temp.next = node;

        return head;

    }

    public static Node insertAtHead(Node head, int x) {
        Node newNode = new Node();
        newNode.data = x;

        if (head == null) {
            head = newNode;
        } else {
            newNode.next = head;
            head = newNode;
        }
        return head;


    }

    /*
     * Problem 4: https://www.hackerrank.com/challenges/insert-a-node-at-a-specific-position-in-a-linked-list/problem
     *
     * */

    public static Node insertNth(Node head, int data, int position) {

        Node node = new Node();
        node.data = data;

        Node start = head;
        Node temp;

        if (position == 0) {
            node.next = head;
            head = node;
            return head;
        }

        for (int i = 1; i < position; i++) {
            start = start.next;
        }

        temp = start.next;
        start.next = node;
        node.next = temp;


        return head;
    }

    /*
     *
     * Problem 5: https://www.hackerrank.com/challenges/delete-a-node-from-a-linked-list/problem
     *
     * */

    public static Node deleteNodeInPosition(Node head, int position) {

        Node temp = head;

        if (position == 0) {
            head = head.next;
            temp.next = null;
            temp = null;
            return head;
        }

        for (int i = 1; i < position; i++) {
            temp = temp.next;
        }

        temp.next = temp.next.next;


        return head;
    }

    /*
     *
     *   Problem 6:   https://www.hackerrank.com/challenges/print-the-elements-of-a-linked-list-in-reverse/problem
     * */

    public static void printReverse(Node head) {
        if (head == null)
            return;
        printReverse(head.next);
        System.out.println(head.data);
    }


    /*
     *
     * Problem 7: Reverse Linked List : https://www.hackerrank.com/challenges/reverse-a-linked-list/problem
     *
     * */


    public static Node reverseLinkedList(Node head) {

        Node prev = head;
        Node current = head.next;

        prev.next = null;

        while (current != null) {
            Node next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }


        return prev;
    }

    /*
    * Problem 8: https://www.hackerrank.com/challenges/compare-two-linked-lists/problem
    *
    * */

    public static int compareTwoLinkList(Node headA, Node headB) {
        int isEqual = 1;

        Boolean doRun = true;

        if (headA==null && headB==null){
            return isEqual;
        }

        while (doRun) {
            if (headA == null && headB == null){
                return isEqual;
            }
            if ((headA == null && headB != null) || (headA != null && headB == null)) {
                isEqual = 0;
                return isEqual;
            }

            if (headA.data == headB.data) {
                headA = headA.next;
                headB = headB.next;
                continue;
            } else {
                isEqual = 0;
                return isEqual;
            }
        }


        return isEqual;
    }


    /*
    *
    *
    * Problem 9: https://www.hackerrank.com/challenges/merge-two-sorted-linked-lists/problem
    *
    * */


    public static Node mergeTwoSortedLinkedList(Node headA, Node headB){

        Node head, currentHead;

        if (headA==null){
            return headB;
        }
        if(headB==null){
            return headA;
        }

        if (headA.data>headB.data){
            head = headB;
            headB = headB.next;
        } else {
            head = headA;
            headA=headA.next;
        }
        head.next = null;
        currentHead = head;

        while (headA!=null && headB!=null){

            Node temp;

            if (headA.data<=headB.data) {

                temp = headA;
                headA = headA.next;

                temp.next = null;

                head.next = temp;
                head=head.next;


            } else {
                temp = headB;
                headB=headB.next;

                temp.next=null;
                head.next = temp;
                head=head.next;
            }
        }

        while (headA!=null){
            head.next = headA;
            head = head.next;
            headA=headA.next;
        }

        while (headB!=null){
            head.next = headB;
            head=head.next;
            headB=headB.next;
        }


        return currentHead;
    }

    /*
    *
    * Problem 10 : https://www.hackerrank.com/challenges/get-the-value-of-the-node-at-a-specific-position-from-the-tail/problem
    *
    * */

    public static Node getNodeValueFromTail(Node head, int position){

        int count=0;

        Node temp=head;
        while (temp!=null){
            temp = temp.next;
            count++;
        }

        System.out.println(count);

        Node result;
        for (int i=1;i<count-position;i++) {
            head  = head.next;
        }

        return head;
    }

    public static Node removeDuplicateFromSortedList(Node head){



        Node newHead = head;
        head = head.next;
        Node top = newHead;

        newHead.next = null;


        Node temp;

        while (head!=null){

            if (top.data == head.data){
                head = head.next;
                continue;
            } else {
                temp = head;
                head = head.next;

                temp.next = null;
                top.next=temp;
                top = top.next;
            }
        }
        return newHead;

    }




}
