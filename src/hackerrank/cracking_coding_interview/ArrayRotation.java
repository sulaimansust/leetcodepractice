package hackerrank.cracking_coding_interview;

/**
 * Created by sulaimankhan on 4/15/2018.
 */
public class ArrayRotation {
    /*
    * Cracking coding interview :
    * Problem 1: https://www.hackerrank.com/challenges/ctci-array-left-rotation/problem
    *
    * */
    public static int[] rotateLeft(int[] input, int num) {
        int tempArray[] = new int[num];

        for (int i = 0; i < num; i++) {
            tempArray[i] = input[i];
        }
        int start = 0;
        for (int i = num; i < input.length; i++) {
            input[i - num] = input[i];
        }
        for (int i = 0; i < num; i++) {
            input[input.length - num + i] = tempArray[i];
        }

        return input;

    }
}
