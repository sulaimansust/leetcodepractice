package hackerrank.cracking_coding_interview;

import common.BinaryNode;
import common.BinaryTree;

import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by sulaimankhan on 4/15/2018.
 */
public class BinarySearchTree {

    public static boolean checkBST(BinaryNode root){


        List<BinaryNode> queue = new LinkedList<>();

        queue.add(root);

        while (!queue.isEmpty()) {
            BinaryNode tempRoot = queue.remove(0);

            if (tempRoot!=null){

                if(tempRoot.left!=null) {
                    if (tempRoot.left.data>tempRoot.data){
                        return false;
                    }
                }
                if (tempRoot.right!=null) {
                    if (tempRoot.right.data<tempRoot.data){
                        return false;
                    }
                }
                queue.add(tempRoot.left);
                queue.add(tempRoot.right);
            }

        }
        return true;
    }

}
