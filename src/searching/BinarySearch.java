package searching;

/**
 * Created by sulaimansust on 6/16/17.
 */
public class BinarySearch {
    public int binarySearch(int arr[], int left, int right, int x) {
        System.out.println("entry call left: "+left+" right: "+right);
        if (right >= left) {
            int mid = (right + left) / 2;
            System.out.println("mid: "+mid);

            // If the element is present at the middle itself
            if (arr[mid] == x)
                return mid;

            // If element is smaller than mid, then it can only
            // be present in left subarray
            if (arr[mid] > x)
                return binarySearch(arr, left, mid - 1, x);

            // Else the element can only be present in right
            // subarray
            return binarySearch(arr, mid + 1, right, x);
        }

        // We reach here when element is not present in array
        return -1;
    }
}
