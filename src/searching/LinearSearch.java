package searching;

/**
 * Created by sulaimansust on 6/16/17.
 */
public class LinearSearch {
    public int linearSearch(int[] input, int searchValue) {
        for (int i = 0; i < input.length; i++) {
            if (input[i] == searchValue) {
                return i;
            }
        }
        return -1;
    }
}
